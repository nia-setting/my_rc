var proxy = "PROXY 127.0.0.1:7890";
var direct = "DIRECT";

function myMatch(string, substring) {
  return string.indexOf(substring) != -1 ? 1 : 0;
}

function isLocalHost(host) {
  return (
    shExpMatch(host, "localhost") ||
    shExpMatch(host, "127.*") ||
    shExpMatch(host, "10.*") ||
    shExpMatch(host, "192.168.*") ||
    shExpMatch(host, "172.16.*") ||
    shExpMatch(host, "[::1]") ||
    shExpMatch(host, "fc00:*") ||
    shExpMatch(host, "fe80:*")
  );
}

function FindProxyForURL(url, host) {
  if (isLocalHost(host) ||
      myMatch(url, "114.212") ||
      myMatch(url, "acm.org") ||
      myMatch(url, "mail.qq") ||
      myMatch(url, "qqmail") ||
      myMatch(url, "baidu") ||
      myMatch(url, "bing") ||
      myMatch(url, "gitee") ||
      myMatch(url, "chsi") ||
      myMatch(url, "csdn") ||
      myMatch(url, "cnblogs") ||
      myMatch(url, "nju") ||
      myMatch(url, "zhihu")) {
    return direct;
  }
  return proxy;
}

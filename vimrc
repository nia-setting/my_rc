" 核心
set tabstop=2 " tab宽度为2
set shiftwidth=2 " 自动缩进2空格
set smartindent " 新的一行自动缩进
set mouse=a " 启用鼠标控制
set hlsearch " 搜索时高亮显示被找到的文本
set number " 显示行号
set cursorline " 突出显示当前行
set laststatus=2 " 显示状态栏 (默认值为 1, 无法显示状态栏)

" 快速切换normal
inoremap jk <esc>
vnoremap jk <esc>
" 退出时防误触
nnoremap ; :
nnoremap q: :
" 快速复制粘贴, vim需要支持粘贴功能
nnoremap <c-a> ggVG
noremap <c-c> "+Y
nnoremap <c-c> "+Y
vnoremap <c-c> "+Y
inoremap <c-v> <esc>"+Pi

" 通用
set showcmd " 显示最后一条执行的命令
set matchtime=2 " 短暂跳转到匹配括号的时间
set magic " 设置魔术: 可以进一步设置正则表达式中哪些符号不需要转义
set foldenable " 开启折叠
set foldmethod=syntax " 设置语法折叠
set foldcolumn=0 " 设置折叠区域的宽度
setlocal foldlevel=1 " 设置折叠层数为 1

" 禁止生成文件
set nobackup
set noswapfile
set nowritebackup

" tokey-metro主题
set t_Co=256
colorscheme my

" 解决中文乱码 ==================================
set fileencodings=utf-8,gb2312,gb18030,gbk,ucs-bom,cp936,latin
set enc=utf8
set fencs=utf8,gbk,gb2312,gb18030
source $VIMRUNTIME/delmenu.vim " gvim菜单乱码
source $VIMRUNTIME/menu.vim
" language messages zh_CN.utf-8

" powerline
set rtp+=/home/lynx/.local/lib/python3.8/site-packages/powerline/bindings/vim

" vim-plug
call plug#begin('~/.vim/plugged')
Plug 'luochen1990/rainbow'
Plug 'darkin-blade/vim-sb-complete2'
Plug 'airblade/vim-gitgutter'
call plug#end()

" rainbow
let g:rainbow_active = 1
let g:rainbow_conf = {
\ 'ctermfgs': ['51','219','230','111','99','33','201','118'],
\}

" gitgutter
let g:gitgutter_max_signs=300
let g:gitgutter_sign_added='++'
let g:gitgutter_sign_removed='->'
let g:gitgutter_sign_modified='~~'
let g:gitgutter_sign_removed_first_line='=>'
let g:gitgutter_sign_modified_removed='+>'
set updatetime=400

" vim-sbcomplete
let g:sbcom2_active = 1
let g:sbcom2_trigger = "<tab>" 
" let g:sbcom1_active = 1
" let g:sbcom1_trigger = "`" 
" let g:sbcom1_maxline = 10000
set pastetoggle=<F10>
 
" 特殊文件类型高亮
au VimEnter * call MyView()
fun! MyView()
  if (expand("%") != "")
    " echom expand("%")
    if !isdirectory(expand("%"))
      silent loadview
      au VimLeave * silent mkview
    endif
  endif

  if (expand("%:e") == "list")
    set filetype=debsources
  elseif (expand("%:e") == "swig")
    set filetype=html
  elseif (expand("%:e") == "ejs")
    set filetype=html
  elseif (expand("%:e") == "styl")
    set filetype=css
  elseif (expand("%:e") == "save")
    set filetype=debsources
  endif

  if ((expand("%:e") != "md")&&(expand("%:e") != "cmm")&&(expand("%:e") != "l"))
    inoremap {<cr> {}<left><cr><esc>O
  else
    inoremap {<cr> {}<left><cr><backspace><esc>O
  endif
endfun
